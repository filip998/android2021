package helper;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import model.Akcija;
import model.Artikal;

public class AkcijaHelper {
    private FirebaseDatabase mDatabase;
    private DatabaseReference mReference;
    private List<Akcija> listaAkcija = new ArrayList<>();

    public interface DataStatus{
        void AkcijaIsLoaded(List<Akcija>akcije,List<String>keys);
        void AkcijaIsInserted();
        void AkcijaIsUpdated();
        void AkcijaIsDeleted();
    }
    public AkcijaHelper(){
        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference("akcije");
    }
    public void readAkcije(final DataStatus dataStatus){
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listaAkcija.clear();
                List<String>keys = new ArrayList<>();
                for (DataSnapshot keyNode : snapshot.getChildren()){
                    keys.add(keyNode.getKey());
                    Akcija akcija = keyNode.getValue(Akcija.class);
                    listaAkcija.add(akcija);
                }
                dataStatus.AkcijaIsLoaded(listaAkcija,keys);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    public void addAkciju(Akcija akcija,final AkcijaHelper.DataStatus dataStatus){
        String key = mReference.push().getKey();
        mReference.child(key).setValue(akcija).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataStatus.AkcijaIsInserted();
            }
        });
    }
    public void updateAkcija(Akcija akcija,String key,final AkcijaHelper.DataStatus dataStatus){
        mReference.child(key).setValue(akcija).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataStatus.AkcijaIsUpdated();
            }
        });
    }
    public void deleteAkcija(String key,final AkcijaHelper.DataStatus dataStatus){
        mReference.child(key).setValue(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataStatus.AkcijaIsDeleted();
            }
        });
    }

}
