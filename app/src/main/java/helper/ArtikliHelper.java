package helper;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import model.Artikal;

public class ArtikliHelper {
    private FirebaseDatabase mDatabase;
    private DatabaseReference mReference;
    private List<Artikal>listaArtikala = new ArrayList<>();

    public interface DataStatus{
        void ArtikalIsLoaded(List<Artikal>artikals,List<String>keys);
        void ArtikalIsInserted();
        void ArtikalIsUpdated();
        void ArtikalIsDeleted();
    }

    public ArtikliHelper(){
        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference("artikli");
    }
    public void readArtikli(final DataStatus dataStatus){
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listaArtikala.clear();
                List<String>keys = new ArrayList<>();
                for (DataSnapshot keyNode : snapshot.getChildren()){
                    keys.add(keyNode.getKey());
                    Artikal artikal = keyNode.getValue(Artikal.class);
                    listaArtikala.add(artikal);
                }
                dataStatus.ArtikalIsLoaded(listaArtikala,keys);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    public void addArtikal(Artikal artikal,final DataStatus dataStatus){
        
        String key = mReference.push().getKey();
        mReference.child(key).setValue(artikal).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataStatus.ArtikalIsInserted();
            }
        });
    }
    public void updateArtikal(Artikal artikal,String key,final DataStatus dataStatus){
        mReference.child(key).setValue(artikal).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataStatus.ArtikalIsUpdated();
            }
        });
    }
    public void deleteArtikal(String key,final DataStatus dataStatus){
        mReference.child(key).setValue(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataStatus.ArtikalIsDeleted();
            }
        });
    }
}
