package adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android2021.R;
import com.example.android2021.activity.UpdateDeletaArtikliActivity;
import com.example.android2021.activity.UpdateDeleteAkcijaActivity;

import java.util.List;

import model.Akcija;
import model.Artikal;

public class RecyclerViewConfigAkcija{
    private Context mcContex;
    private AkcijaAdapter akcijaAdapter;
    public void setConfig(RecyclerView recyclerView,Context context,List<Akcija>akcije,List<String>keys){
        mcContex = context;
        akcijaAdapter = new AkcijaAdapter(akcije,keys);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(akcijaAdapter);
    }

    class AkcijaItemView extends RecyclerView.ViewHolder{
        private TextView tProcenat;
        private TextView tOdKad;
        private TextView tDoKad;
        private TextView tTekst;

        private String key;

        public AkcijaItemView(ViewGroup parent) {
            super(LayoutInflater.from(mcContex).inflate(R.layout.rowakcije,parent,false));
            tProcenat = (TextView)itemView.findViewById(R.id.procenat);
            tOdKad = (TextView)itemView.findViewById(R.id.odKad);
            tDoKad = (TextView)itemView.findViewById(R.id.doKad);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcContex, UpdateDeleteAkcijaActivity.class);
                    intent.putExtra("key",key);
                    intent.putExtra("procenat",tProcenat.getText().toString());
                    intent.putExtra("odKad",tOdKad.getText().toString());
                    intent.putExtra("DoKad",tDoKad.getText().toString());
                    mcContex.startActivity(intent);
                }
            });
        }
        public void bind(Akcija akcija, String key){
            tProcenat.setText(akcija.getProcenat());
            tOdKad.setText(akcija.getOdKad());
            tDoKad.setText(akcija.getDoKad());
            this.key = key;
        }

    }
    class AkcijaAdapter extends RecyclerView.Adapter<AkcijaItemView>{
        private List<Akcija> listaAkcija;
        private List<String>keys;

        public AkcijaAdapter(List<Akcija> listaAkcija, List<String> keys) {
            this.listaAkcija = listaAkcija;
            this.keys = keys;
        }

        public AkcijaAdapter() {
            super();
        }

        @NonNull
        @Override
        public AkcijaItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new AkcijaItemView(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull AkcijaItemView holder, int position) {
            holder.bind(listaAkcija.get(position),keys.get(position));
        }

        @Override
        public int getItemCount() {
            return listaAkcija.size();
        }
    }
}