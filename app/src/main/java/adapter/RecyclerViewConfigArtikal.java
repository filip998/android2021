package adapter;


import android.content.Context;

import android.content.Intent;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android2021.R;
import com.example.android2021.activity.UpdateDeletaArtikliActivity;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;


import java.util.List;

import model.Artikal;

public class RecyclerViewConfigArtikal{
    private Context mcontext;
    private ArtikalAdapter artikalAdapter;
    public void setConfig(RecyclerView recyclerView,Context context,List<Artikal>artikli,List<String>keys){
        mcontext = context;
        artikalAdapter = new ArtikalAdapter(artikli,keys);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(artikalAdapter);
    }


    class ArtikalItemView extends RecyclerView.ViewHolder{
        private TextView tNaziv;
        private TextView tOpis;
        private TextView tCena;
        private ImageView tUrl;

        private String key;

        public ArtikalItemView(ViewGroup parent){
            super(LayoutInflater.from(mcontext).inflate(R.layout.rowartikli,parent,false));
            tNaziv = (TextView)itemView.findViewById(R.id.nazivTxt);
            tOpis = (TextView)itemView.findViewById(R.id.opisTxt);
            tCena = (TextView)itemView.findViewById(R.id.cenaTxt);


             
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mcontext, UpdateDeletaArtikliActivity.class);
                    intent.putExtra("key",key);
                    intent.putExtra("naziv",tNaziv.getText().toString());
                    intent.putExtra("opis",tOpis.getText().toString());
                    intent.putExtra("cena",tCena.getText().toString());
                    mcontext.startActivity(intent);

                }
            });


        }
        public void bind(Artikal artikal,String key){
            tNaziv.setText(artikal.getNaziv());
            tOpis.setText(artikal.getOpis());
            tCena.setText(artikal.getCena());
            this.key = key;
        }

    }
    private class ArtikalAdapter extends RecyclerView.Adapter<ArtikalItemView>{
        private List<Artikal>listaArtikala;
        private List<String>keys;

        public ArtikalAdapter(List<Artikal> listaArtikala, List<String> keys) {
            this.listaArtikala = listaArtikala;
            this.keys = keys;
        }

        public ArtikalAdapter() {
            super();
        }

        @NonNull
        @Override
        public ArtikalItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ArtikalItemView(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull ArtikalItemView holder, int position) {
            holder.bind(listaArtikala.get(position),keys.get(position));
        }

        @Override
        public int getItemCount() {
            return listaArtikala.size();
        }
    }

}

