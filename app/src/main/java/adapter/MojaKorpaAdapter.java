package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android2021.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import model.ArtikalKorpa;

public class MojaKorpaAdapter extends RecyclerView.Adapter<MojaKorpaAdapter.ViewHolder> {
    Context context;
    List<ArtikalKorpa>lisaArtikala;
    int totalnaCena = 0;
    FirebaseAuth auth;
    FirebaseFirestore firestore;

    public MojaKorpaAdapter(Context context, List<ArtikalKorpa> lisaArtikala) {
        this.context = context;
        this.lisaArtikala = lisaArtikala;
        firestore = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.moja_korpa,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.naziv.setText(lisaArtikala.get(position).getNazivProizvoda());
        holder.cena.setText(lisaArtikala.get(position).getCenaProizvoda());
        holder.datum.setText(lisaArtikala.get(position).getTrenutniDatum());
        holder.vreme.setText(lisaArtikala.get(position).getTrenutnoVreme());
        holder.kolicina.setText(lisaArtikala.get(position).getTotalnaKolicina());
        holder.totalnaCena.setText(String.valueOf(lisaArtikala.get(position).getTotalnaCena()));
        holder.obrisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firestore.collection("TrenutniKorisnik").document(auth.getCurrentUser().getUid())
                        .collection("DodajUKorpu").document(lisaArtikala.get(position).getIdProizvoda())
                        .delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                       if (task.isSuccessful()){
                           lisaArtikala.remove(lisaArtikala.get(position));
                           notifyDataSetChanged();
                           Toast.makeText(context,"Stavka obrisana!",Toast.LENGTH_SHORT).show();
                       }else {
                           Toast.makeText(context,"Greska!",Toast.LENGTH_SHORT).show();
                       }
                    }
                });
            }
        });

        totalnaCena = totalnaCena + lisaArtikala.get(position).getTotalnaCena();
        Intent intent = new Intent("MojaTotalnaCena");
        intent.putExtra("totalnaCena",totalnaCena);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }

    @Override
    public int getItemCount() {
        return lisaArtikala.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView obrisi;
        TextView  naziv,cena,datum,vreme,kolicina,totalnaCena;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            naziv = itemView.findViewById(R.id.nazivArtikla);
            cena = itemView.findViewById(R.id.cenaArtikla);
            datum = itemView.findViewById(R.id.datum);
            vreme = itemView.findViewById(R.id.vreme);
            kolicina = itemView.findViewById(R.id.totalnaKolicina);
            totalnaCena = itemView.findViewById(R.id.totalnaCena);
            obrisi = itemView.findViewById(R.id.obrisiIzKorpe);
        }
    }
}
