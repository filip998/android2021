package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.android2021.R;
import com.example.android2021.activity.ArtikalDetalj;

import java.util.List;

import model.ArtkilalKupac;

public class ArtikliKupacAdapter extends RecyclerView.Adapter<ArtikliKupacAdapter.ViewHolder> {
    Context context;
    List<ArtkilalKupac>listaArtikala;

    public ArtikliKupacAdapter(Context context, List<ArtkilalKupac> listaArtikala) {
        this.context = context;
        this.listaArtikala = listaArtikala;
    }

    @NonNull
    @Override
    public ArtikliKupacAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rowartiklikupac,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ArtikliKupacAdapter.ViewHolder holder, int position) {
        Glide.with(context).load(listaArtikala.get(position).getSlika()).into(holder.imageView);
        holder.naziv.setText(listaArtikala.get(position).getNaziv());
        holder.opis.setText(listaArtikala.get(position).getOpis());
        holder.cena.setText(listaArtikala.get(position).getCena()+" din");
        holder.rejting.setText(listaArtikala.get(position).getRejting());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ArtikalDetalj.class);
                intent.putExtra("detail",listaArtikala.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listaArtikala.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView naziv,opis,cena,rejting;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.slikaArtiklaKupac);
            naziv = itemView.findViewById(R.id.artikalKNaziv);
            opis = itemView.findViewById(R.id.artikalKOpis);
            cena = itemView.findViewById(R.id.artikalKCena);
            rejting = itemView.findViewById(R.id.rating);


        }
    }
}
