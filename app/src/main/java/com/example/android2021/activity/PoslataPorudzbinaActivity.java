package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.example.android2021.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import model.ArtikalKorpa;

public class PoslataPorudzbinaActivity extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseFirestore firestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poslata_porudzbina);

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        List<ArtikalKorpa> list = (ArrayList<ArtikalKorpa>) getIntent().getSerializableExtra("listaArtikala");

        if (list != null && list.size() > 0){
            for (ArtikalKorpa korpa : list){
                final HashMap<String,Object> cartMap = new HashMap<>();
                cartMap.put("nazivProizvoda",korpa.getNazivProizvoda());
                cartMap.put("cenaProizvoda",korpa.getCenaProizvoda());
                cartMap.put("trenutniDatum",korpa.getTrenutniDatum());
                cartMap.put("trenutnoVreme",korpa.getTrenutnoVreme());
                cartMap.put("totalnaKolicina",korpa.getTotalnaKolicina());
                cartMap.put("totalnaCena",korpa.getTotalnaCena());

                firestore.collection("TrenutniKorisnik").document(auth.getCurrentUser().getUid())
                        .collection("MojaPorudzbina").add(cartMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        Toast.makeText(PoslataPorudzbinaActivity.this,"Vasa porudzbina je poslata!",Toast.LENGTH_SHORT).show();

                    }
                });
            }
        }
    }
}