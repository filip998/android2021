package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android2021.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.List;

import helper.ArtikliHelper;
import model.Artikal;

public class DodajArtikalActivity extends AppCompatActivity {
    private ImageView slikaArtikla;
    DatabaseReference databaseReference;
    String userId;
    EditText nazivInput,opisInput,cenaInput;
    Button btnDodaj;
    private FirebaseStorage storage;
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private FirebaseUser user;
    private DatabaseReference reference;
    private static final int IMF_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_artikal);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();


        reference = FirebaseDatabase.getInstance().getReference("artikli");

        nazivInput = findViewById(R.id.txtNazivDodaj);
        opisInput = findViewById(R.id.txtOpisDodaj);
        cenaInput = findViewById(R.id.txtCenaDodaj);
        btnDodaj = findViewById(R.id.btnDodajArtikal);
        slikaArtikla = findViewById(R.id.artikalDodajSlikuId);

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("users");
        userId = user.getUid();


        btnDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Artikal artikal = new Artikal();
                artikal.setNaziv(nazivInput.getText().toString());
                artikal.setOpis(opisInput.getText().toString());
                artikal.setCena(cenaInput.getText().toString()+ " din");
                artikal.setPutanjaSlike(slikaArtikla.toString());


                artikal.setUserId(userId);
                new ArtikliHelper().addArtikal(artikal, new ArtikliHelper.DataStatus() {
                    @Override
                    public void ArtikalIsLoaded(List<Artikal> artikals, List<String> keys) {

                    }

                    @Override
                    public void ArtikalIsInserted() {
                        Toast.makeText(DodajArtikalActivity.this,"Uspesno kreiran artikal!",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void ArtikalIsUpdated() {

                    }

                    @Override
                    public void ArtikalIsDeleted() {

                    }
                });

            }
        });
        slikaArtikla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions,PERMISSION_CODE);
                    }
                    else {
                        pickImageFromGallery();
                    }
                }
                else {
                    pickImageFromGallery();
                }
            }
        });
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent,IMF_PICK_CODE);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case PERMISSION_CODE:{
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery();
                }else {
                    Toast.makeText(this,"Permission denied",Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri profilUri = data.getData();
        if (resultCode == RESULT_OK && requestCode == IMF_PICK_CODE){
            slikaArtikla.setImageURI(profilUri);
            final StorageReference reference = storage.getReference().child("artikliPhotos").child(FirebaseAuth.getInstance().getUid());

            reference.putFile(profilUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(DodajArtikalActivity.this,"Uspesno dodavanje slike!",Toast.LENGTH_SHORT).show();
                    reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String key = mDatabase.getReference().child("artikli").push().getKey();
                            mDatabase.getReference().child("artikli").child(key).child("putanjaSlike").setValue(uri.toString());

                            Toast.makeText(DodajArtikalActivity.this,"Slika artikla dodata!",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }
    }
}