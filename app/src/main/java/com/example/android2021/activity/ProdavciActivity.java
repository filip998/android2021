package com.example.android2021.activity;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


import com.example.android2021.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import adapter.ArtikliKupacAdapter;
import model.ArtkilalKupac;

public class ProdavciActivity extends AppCompatActivity {

    FirebaseFirestore db;
    RecyclerView recyclerView;
    ArrayList<ArtkilalKupac>listaArtikala;
    ArtikliKupacAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prodavci);

        Toolbar toolbar = findViewById(R.id.toolbarProdavci);
        setSupportActionBar(toolbar);
        db = FirebaseFirestore.getInstance();
        recyclerView = findViewById(R.id.recycleViewProdavci);
        recyclerView.setLayoutManager(new LinearLayoutManager(ProdavciActivity.this,LinearLayoutManager.VERTICAL,false));
        listaArtikala = new ArrayList<>();
        adapter = new ArtikliKupacAdapter(ProdavciActivity.this,listaArtikala);
        recyclerView.setAdapter(adapter);

        db.collection("artikli").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for (DocumentSnapshot snapshot : task.getResult().getDocuments()){
                        ArtkilalKupac artkilalKupac = snapshot.toObject(ArtkilalKupac.class);
                        listaArtikala.add(artkilalKupac);
                        adapter.notifyDataSetChanged();

                    }
                }else {
                    Toast.makeText(ProdavciActivity.this,"Greska" + task.getException(),Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            recreate();
        }
    }

}