package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android2021.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import model.Kupac;
import model.Prodavac;

public class RegistracijaActivity extends AppCompatActivity {

    Button btnNRegistracija;
    EditText ime,prezime,email,password;
    private FirebaseAuth mAuth;
    Spinner uloga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registracija);
        mAuth = FirebaseAuth.getInstance();

        btnNRegistracija = findViewById(R.id.btnRegistrujSe);

        ime = findViewById(R.id.txtImeReg);
        prezime = findViewById(R.id.txtPrezimeReg);
        email = findViewById(R.id.txtUsernameReg);
        password = findViewById(R.id.txtPasswordReg);
        uloga = findViewById(R.id.spinnerUloga);


        btnNRegistracija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dodajKorisnika();
            }
        });
    }

    private void dodajKorisnika(){
        final String id = mAuth.getUid();
        final String name = ime.getText().toString().trim();
        final String lastname = prezime.getText().toString().trim();
        final String eMail = email.getText().toString().trim();
        final String pass = password.getText().toString().trim();
        final String userType = uloga.getSelectedItem().toString().trim();

        if (name.isEmpty()){
            ime.setError("Unesite vase ime!");
            ime.requestFocus();
            return;
        }
        if (lastname.isEmpty()){
            prezime.setError("Unesite vase prezime!");
            prezime.requestFocus();
            return;
        }
        if (eMail.isEmpty()){
            email.setError("Unesite vas email!");
            email.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(eMail).matches()){
            email.setError("Unesite validan email!");
        }
        if (pass.isEmpty()){
            password.setError("Unesite vasu lozinku!");
            password.requestFocus();
            return;
        }
        if (pass.length()<6){
            password.setError("Lozinka mora biti minimum 6 karaktera!");
            password.requestFocus();
            return;
        }
        mAuth.createUserWithEmailAndPassword(eMail,pass).addOnCompleteListener(RegistracijaActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                String imageUrl = "https://www.iconpacks.net/icons/1/free-user-icon-244-thumb.png";
                if (task.isSuccessful()){
                    if (userType.equals("prodavac")) {
                        String tipKorisnika = userType;

                        Prodavac prodavac = new Prodavac(id, name, lastname, eMail, pass, imageUrl,false, tipKorisnika,null, null, null, null, null, null);
                        FirebaseDatabase.getInstance().getReference("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .setValue(prodavac).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(RegistracijaActivity.this, "Uspesna registracija prodavca!", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(RegistracijaActivity.this,"Neuspena regisracija prodavca!",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }else if (userType.equals("kupac")){
                        String tipKorisnika = userType;
                        Kupac kupac = new Kupac(id,name,lastname,eMail,pass,imageUrl,false,tipKorisnika,null,null);
                        FirebaseDatabase.getInstance().getReference("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(kupac).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(RegistracijaActivity.this, "Uspesna registracija kupca!", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(RegistracijaActivity.this,"Neuspena regisracija kupca!",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            }
        });

    }
}