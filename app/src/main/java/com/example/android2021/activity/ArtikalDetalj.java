package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.android2021.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import model.ArtkilalKupac;

public class ArtikalDetalj extends AppCompatActivity {

    TextView kolicina;
    int totalnaKolicina = 1;
    int totalnaCena;
    ImageView detaljnaSlika;
    TextView opis,cena,rejting;
    Button btnDodajUKorpu;
    ImageView dodajKolicinu,smanjiKolicinu;
    ArtkilalKupac artkilalKupac = null;

    FirebaseFirestore firestore;
    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artikal_detalj);
        Toolbar toolbar = findViewById(R.id.toolbarProdavci);
        setSupportActionBar(toolbar);
        firestore = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();


        final Object object = getIntent().getSerializableExtra("detail");
        if (object instanceof ArtkilalKupac){
            artkilalKupac = (ArtkilalKupac)object;
        }


        detaljnaSlika = findViewById(R.id.detaljaSlika);
        opis = findViewById(R.id.tvDescription);
        cena = findViewById(R.id.detaljnaCena);
        rejting = findViewById(R.id.detaljanRejting);
        btnDodajUKorpu = findViewById(R.id.dodajUKorpu);
        dodajKolicinu = findViewById(R.id.dodajArtikal);
        smanjiKolicinu = findViewById(R.id.smanjiArtikal);
        kolicina  = findViewById(R.id.kolicina);

        if (artkilalKupac != null){
            Glide.with(getApplicationContext()).load(artkilalKupac.getSlika()).into(detaljnaSlika);
            opis.setText(artkilalKupac.getOpis());
            cena.setText("Cena :"+artkilalKupac.getCena() + " din");
            rejting.setText(artkilalKupac.getRejting());



        }

        dodajKolicinu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (totalnaKolicina < 10){
                    totalnaKolicina++;
                    totalnaCena = artkilalKupac.getCena() * totalnaKolicina;
                    kolicina.setText(String.valueOf(totalnaKolicina));

                }
            }
        });
        smanjiKolicinu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (totalnaKolicina > 0){
                    totalnaKolicina--;
                    totalnaCena = artkilalKupac.getCena() * totalnaKolicina;
                    kolicina.setText(String.valueOf(totalnaKolicina));

                }
            }
        });
        btnDodajUKorpu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dodajUKorpu();
            }
        });

    }

    private void dodajUKorpu() {
        String sacuvajDatum,sacuvajVreme;
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat trenutanDatum = new  SimpleDateFormat("dd MM, yyyy");
        sacuvajDatum = trenutanDatum.format(calendar.getTime());

        SimpleDateFormat trenutnoVreme = new  SimpleDateFormat("HH:mm:ss a");
        sacuvajVreme = trenutnoVreme.format(calendar.getTime());

        final HashMap<String,Object>cartMap = new HashMap<>();
        cartMap.put("nazivProizvoda",artkilalKupac.getNaziv());
        cartMap.put("cenaProizvoda",cena.getText().toString());
        cartMap.put("trenutniDatum",sacuvajDatum);
        cartMap.put("trenutnoVreme",sacuvajVreme);
        cartMap.put("totalnaKolicina",kolicina.getText().toString());
        cartMap.put("totalnaCena",totalnaCena);

        firestore.collection("TrenutniKorisnik").document(auth.getCurrentUser().getUid())
                .collection("DodajUKorpu").add(cartMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                Toast.makeText(ArtikalDetalj.this,"Dodato u korpu!",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}