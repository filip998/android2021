package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;



import com.example.android2021.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.List;


import adapter.RecyclerViewConfigArtikal;
import helper.ArtikliHelper;
import model.Artikal;

public class    ArtikliActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    private DatabaseReference databaseReference;
    private FirebaseUser user;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artikli);
        Toolbar toolbar = findViewById(R.id.toolbarArtikli);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recycleViewArtikli);
        new ArtikliHelper().readArtikli(new ArtikliHelper.DataStatus() {
            @Override
            public void ArtikalIsLoaded(List<Artikal> artikals, List<String> keys) {
                new RecyclerViewConfigArtikal().setConfig(recyclerView,ArtikliActivity.this,artikals,keys);
            }

            @Override
            public void ArtikalIsInserted() {

            }

            @Override
            public void ArtikalIsUpdated() {

            }

            @Override
            public void ArtikalIsDeleted() {

            }
        });

        user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("users");
        userId = user.getUid();



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            recreate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_edit_delete_menu,menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.dodaj){
            Intent intent = new Intent(ArtikliActivity.this,DodajArtikalActivity.class);
            startActivity(intent);
        }
        return true;
    }

}