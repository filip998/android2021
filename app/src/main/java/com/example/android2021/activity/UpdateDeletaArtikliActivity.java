package com.example.android2021.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android2021.R;

import java.util.List;

import helper.ArtikliHelper;
import model.Artikal;

public class UpdateDeletaArtikliActivity extends AppCompatActivity {

    private EditText nazivUp,opisUp,cenaUp;
    private Button btnIzmeni,btnObrisi;

    private String key,naziv,opis,cena;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_deleta_artikli);

        key = getIntent().getStringExtra("key");
        naziv = getIntent().getStringExtra("naziv");
        opis = getIntent().getStringExtra("opis");
        cena = getIntent().getStringExtra("cena");

        nazivUp = findViewById(R.id.txtNazivIzmeni);
        nazivUp.setText(naziv);
        opisUp = findViewById(R.id.txtOpisIzmeni);
        opisUp.setText(opis);
        cenaUp = findViewById(R.id.txtCenaIzmeni);
        cenaUp.setText(cena);
        btnIzmeni = findViewById(R.id.btnIzmeniArtikal);
        btnObrisi = findViewById(R.id.btnObrisiArtikal);

        btnIzmeni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Artikal artikal = new Artikal();
                artikal.setNaziv(nazivUp.getText().toString());
                artikal.setOpis(opisUp.getText().toString());
                artikal.setCena(cenaUp.getText().toString());

                new ArtikliHelper().updateArtikal(artikal, key, new ArtikliHelper.DataStatus() {
                    @Override
                    public void ArtikalIsLoaded(List<Artikal> artikals, List<String> keys) {

                    }

                    @Override
                    public void ArtikalIsInserted() {

                    }

                    @Override
                    public void ArtikalIsUpdated() {
                        Toast.makeText(UpdateDeletaArtikliActivity.this,"Artikal uspesno izmenjen",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void ArtikalIsDeleted() {

                    }
                });
            }
        });
        btnObrisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ArtikliHelper().deleteArtikal(key, new ArtikliHelper.DataStatus() {
                    @Override
                    public void ArtikalIsLoaded(List<Artikal> artikals, List<String> keys) {

                    }

                    @Override
                    public void ArtikalIsInserted() {

                    }

                    @Override
                    public void ArtikalIsUpdated() {

                    }

                    @Override
                    public void ArtikalIsDeleted() {
                        Toast.makeText(UpdateDeletaArtikliActivity.this,"Artikal uspesno obrisan!",Toast.LENGTH_SHORT).show();
                        finish(); return;
                    }
                });
            }
        });



    }


}