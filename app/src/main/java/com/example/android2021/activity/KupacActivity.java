package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


import com.example.android2021.R;
import com.google.firebase.auth.FirebaseAuth;

public class KupacActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kupac);


        Toolbar toolbar = findViewById(R.id.toolbarKupac);
        setSupportActionBar(toolbar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.odjava){
            FirebaseAuth.getInstance().signOut();
            Intent inten = new Intent(KupacActivity.this,MainActivity.class);
            startActivity(inten);
        }else if(id == R.id.poruci){
            Intent intent = new Intent(KupacActivity.this,ProdavciActivity.class);
            startActivity(intent);
        }else if(id == R.id.mojProfilK){
            startActivity(new Intent(KupacActivity.this,MojProfilActivity.class));
        }else if (id == R.id.mojePorudzbine){
            startActivity(new Intent(KupacActivity.this,MojaKorpaActivity.class));
        }
        return true;
    }
}