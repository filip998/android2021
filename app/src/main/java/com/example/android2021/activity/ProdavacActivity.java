package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.android2021.R;
import com.google.firebase.auth.FirebaseAuth;

public class ProdavacActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prodavac);

        Toolbar toolbar = findViewById(R.id.toolbarProdavac);
        setSupportActionBar(toolbar);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.prodavacmenu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.artikli){
            Intent intent = new Intent(ProdavacActivity.this,ArtikliActivity.class);
            startActivity(intent);
        }else if(id == R.id.akcije){
            Intent intent = new Intent(ProdavacActivity.this,AkcijeActivity.class);
            startActivity(intent);
        }else if(id == R.id.odjavaPro){
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(ProdavacActivity.this,MainActivity.class);
            startActivity(intent);
        }else if(id == R.id.mojProfilPro){

            Intent intent = new Intent(ProdavacActivity.this,MojProfilActivity.class);
            startActivity(intent);
        }
        return true;
    }

}