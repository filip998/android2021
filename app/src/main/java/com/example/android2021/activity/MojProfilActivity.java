package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.android2021.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import de.hdodenhof.circleimageview.CircleImageView;
import model.Korisnik;

public class MojProfilActivity extends AppCompatActivity {

    private CircleImageView profileImg;
    private EditText name,lastname,username,password;
    private FirebaseStorage storage;
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private FirebaseUser user;
    private DatabaseReference reference;
    private String userId;

    private static final int IMF_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moj_profil2);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();

        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("users");
        userId = user.getUid();
        name = findViewById(R.id.profilename);
        lastname = findViewById(R.id.profileLastname);
        username = findViewById(R.id.profilKorIme);
        password = findViewById(R.id.profilLozinka);
        profileImg = findViewById(R.id.profilnaSlika);


        reference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Korisnik userProfile = snapshot.getValue(Korisnik.class);
                if (userProfile!= null){
                    String ime = userProfile.getIme();
                    String prezime = userProfile.getPrezime();
                    String korIme = userProfile.getUsername();
                    String lozinka = userProfile.getPassword();
                    String slika = snapshot.child("profileImage").getValue().toString();
                    Glide.with(MojProfilActivity.this).load(slika).into(profileImg);


                    name.setText(ime);
                    lastname.setText(prezime);
                    username.setText(korIme);
                    password.setText(lozinka);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MojProfilActivity.this,"Doslo je do greske!",Toast.LENGTH_SHORT).show();
            }
        });
        profileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions,PERMISSION_CODE);
                    }
                    else {
                        pickImageFromGallery();
                    }
                }
                else {
                    pickImageFromGallery();
                }
            }
        });


    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent,IMF_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case PERMISSION_CODE:{
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery();
                }else {
                    Toast.makeText(this,"Permission denied",Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri profilUri = data.getData();
        if (resultCode == RESULT_OK && requestCode == IMF_PICK_CODE){
            profileImg.setImageURI(profilUri);
            final StorageReference reference = storage.getReference().child("profilePhotos").child(userId);

            reference.putFile(profilUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(MojProfilActivity.this,"Uspesno dodavanje slike!",Toast.LENGTH_SHORT).show();
                    reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            mDatabase.getReference().child("users").child(FirebaseAuth.getInstance().getUid()).child("profileImage").setValue(uri.toString());

                            Toast.makeText(MojProfilActivity.this,"Profilna slika dodata!",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }
    }
}