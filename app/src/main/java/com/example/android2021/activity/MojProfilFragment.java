package com.example.android2021.activity;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android2021.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;


public class MojProfilFragment extends Fragment {

    CircleImageView profileImg;
    EditText name,lastname,username,password;
    FirebaseStorage storage;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;

    private static final int IMF_PICK_CODE = 1000;
    private static final int PREMISSION_CODE = 1000;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_moj_profil,container,false);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
        profileImg = root.findViewById(R.id.profilnaSlika);
        name = root.findViewById(R.id.profilename);
        lastname = root.findViewById(R.id.profileLastname);
        username = root.findViewById(R.id.profilKorIme);
        password = root.findViewById(R.id.profilLozinka);

       profileImg.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                   if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_DENIED){
                       String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                       requestPermissions(permissions,PREMISSION_CODE);
                   }else {
                       pickImageFromGallery();
                   }

               }else {
                   pickImageFromGallery();
               }
           }
       });

        return root;
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent,IMF_PICK_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == getActivity().RESULT_OK && requestCode == IMF_PICK_CODE){
            profileImg.setImageURI(data.getData());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PREMISSION_CODE:{
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery();
                }else {
                    Toast.makeText(getActivity(),"Permission denied",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}