package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android2021.R;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

import adapter.RecyclerViewConfigAkcija;
import adapter.RecyclerViewConfigArtikal;
import helper.AkcijaHelper;
import model.Akcija;

public class AkcijeActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    private DatabaseReference databaseReference;
    private FirebaseUser user;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akcije);

        Toolbar toolbar = findViewById(R.id.toolbarAkcije);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recycleViewAkcije);
        new AkcijaHelper().readAkcije(new AkcijaHelper.DataStatus() {
            @Override
            public void AkcijaIsLoaded(List<Akcija> akcije, List<String> keys) {
                new RecyclerViewConfigAkcija().setConfig(recyclerView,AkcijeActivity.this,akcije,keys);
            }

            @Override
            public void AkcijaIsInserted() {

            }

            @Override
            public void AkcijaIsUpdated() {

            }

            @Override
            public void AkcijaIsDeleted() {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_edit_delete_menu,menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.dodaj){
            Intent intent = new Intent(AkcijeActivity.this,DodajAkcijuActivity.class);
            startActivity(intent);
        }
        return true;
    }
}