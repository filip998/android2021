package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.android2021.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import adapter.ArtikliKupacAdapter;
import adapter.MojaKorpaAdapter;
import model.ArtikalKorpa;

public class MojaKorpaActivity extends AppCompatActivity {

    FirebaseFirestore firestore;
    FirebaseAuth auth;
    RecyclerView recyclerView;
    MojaKorpaAdapter adapter;
    List<ArtikalKorpa>korpaList;
    TextView overTotalAmount;
    Button btnKupi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moja_korpa);

        firestore = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        recyclerView = findViewById(R.id.recycleViewKorpa);
        recyclerView.setLayoutManager(new LinearLayoutManager(MojaKorpaActivity.this,LinearLayoutManager.VERTICAL,false));
        overTotalAmount = findViewById(R.id.tvTotalCena);
        btnKupi = findViewById(R.id.kupiOdmah);

        LocalBroadcastManager.getInstance(MojaKorpaActivity.this).registerReceiver(broadcastReceiver,new IntentFilter("MojaTotalnaCena"));
        korpaList = new ArrayList<>();
        adapter = new MojaKorpaAdapter(MojaKorpaActivity.this,korpaList);
        recyclerView.setAdapter(adapter);

        firestore.collection("TrenutniKorisnik").document(auth.getCurrentUser().getUid())
                .collection("DodajUKorpu").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()){
                        String documentId = documentSnapshot.getId();
                        ArtikalKorpa artikalKorpa = documentSnapshot.toObject(ArtikalKorpa.class);
                        artikalKorpa.setIdProizvoda(documentId);
                        korpaList.add(artikalKorpa);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
        btnKupi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MojaKorpaActivity.this,PoslataPorudzbinaActivity.class);
                intent.putExtra("listaArtikala",(Serializable) korpaList);
                startActivity(intent);
            }
        });
    }
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int totalBill = intent.getIntExtra("totalnaCena",0);
            overTotalAmount.setText("Totalni racun: " + totalBill + " din");

        }
    };
}