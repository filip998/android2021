package com.example.android2021.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android2021.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText username;
    EditText  password;

    TextView registracija;
    Button btnPrijava;

    private FirebaseAuth mAuth;
    private DatabaseReference mref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();

        username =findViewById(R.id.txtUsername);
        password = findViewById(R.id.txtPassword);
        registracija = findViewById(R.id.tvRegistracija);

        btnPrijava = findViewById(R.id.btnPrijava);
        btnPrijava.setOnClickListener(this);

        registracija.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvRegistracija:
                startActivity(new Intent(MainActivity.this,RegistracijaActivity.class));
                break;
            case R.id.btnPrijava:
                prijava();
                break;
        }
    }

    private void prijava() {
        String email = username.getText().toString().trim();
        String pass = password.getText().toString().trim();

        if (email.isEmpty()){
            username.setError("Unesite vas email!");
            username.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            username.setError("Unesite validan Email!");
            username.requestFocus();
            return;
        }
        if (pass.isEmpty()){
            password.setError("Unesite vasu lozinku!");
            password.requestFocus();
            return;
        }
        mAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(MainActivity.this,new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    String userid = user.getUid();
                    mref = FirebaseDatabase.getInstance().getReference("users").child(userid).child("uloga");
                    mref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            String uloga = snapshot.getValue(String.class);
                            if (uloga.equals("prodavac")){
                                startActivity(new Intent(MainActivity.this,ProdavacActivity.class));
                            }else if(uloga.equals("kupac")){
                                startActivity(new Intent(MainActivity.this,KupacActivity.class));
                            }else if (uloga.equals("admin")){
                                startActivity(new Intent(MainActivity.this,AdminActivity.class));
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Toast.makeText(MainActivity.this,"Ne postoji ta uloga!",Toast.LENGTH_SHORT).show();
                        }
                    });
                }else {
                    Toast.makeText(MainActivity.this,"Pogresni login podaci!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}