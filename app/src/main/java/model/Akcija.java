package model;

import java.util.Date;

public class Akcija {

    private String procenat;
    private String odKad;
    private String doKad;
    private String tekst;

    public Akcija() {
    }

    public Akcija(String procenat, String odKad, String doKad, String tekst) {
        this.procenat = procenat;
        this.odKad = odKad;
        this.doKad = doKad;
        this.tekst = tekst;
    }

    public String getProcenat() {
        return procenat;
    }

    public void setProcenat(String procenat) {
        this.procenat = procenat;
    }

    public String getOdKad() {
        return odKad;
    }

    public void setOdKad(String odKad) {
        this.odKad = odKad;
    }

    public String getDoKad() {
        return doKad;
    }

    public void setDoKad(String doKad) {
        this.doKad = doKad;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }
}
