package model;

import java.util.ArrayList;

public class Kupac extends Korisnik {

    private String adresa;
    private ArrayList<Porudzbina>porudzbine;

    public Kupac(String id, String ime, String prezime, String username, String password, String profileImage, boolean blokiran, String uloga, String adresa, ArrayList<Porudzbina> porudzbine) {
        super(id, ime, prezime, username, password, profileImage, blokiran, uloga);
        this.adresa = adresa;
        this.porudzbine = porudzbine;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public ArrayList<Porudzbina> getPorudzbine() {
        return porudzbine;
    }

    public void setPorudzbine(ArrayList<Porudzbina> porudzbine) {
        this.porudzbine = porudzbine;
    }
}

