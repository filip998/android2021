package model;

import java.io.Serializable;

public class ArtkilalKupac implements Serializable {
    private String naziv;
    private String opis;
    private int cena;
    private String slika;
    private String rejting;

    public ArtkilalKupac() {
    }

    public ArtkilalKupac(String naziv, String opis, int cena, String slika, String rejting) {
        this.naziv = naziv;
        this.opis = opis;
        this.cena = cena;
        this.slika = slika;
        this.rejting = rejting;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public String getRejting() {
        return rejting;
    }

    public void setRejting(String rejting) {
        this.rejting = rejting;
    }
}
