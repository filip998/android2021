package model;

import java.io.Serializable;

public class ArtikalKorpa implements Serializable {

    private String nazivProizvoda;
    private String cenaProizvoda;
    private String trenutniDatum;
    private String trenutnoVreme;
    private String totalnaKolicina;
    private int totalnaCena;
    String idProizvoda;

    public ArtikalKorpa() {
    }

    public ArtikalKorpa(String nazivProizvoda, String cenaProizvoda, String trenutniDatum, String trenutnoVreme, String totalnaKolicina, int totalnaCena) {
        this.nazivProizvoda = nazivProizvoda;
        this.cenaProizvoda = cenaProizvoda;
        this.trenutniDatum = trenutniDatum;
        this.trenutnoVreme = trenutnoVreme;
        this.totalnaKolicina = totalnaKolicina;
        this.totalnaCena = totalnaCena;
    }

    public String getIdProizvoda() {
        return idProizvoda;
    }

    public void setIdProizvoda(String idProizvoda) {
        this.idProizvoda = idProizvoda;
    }

    public String getNazivProizvoda() {
        return nazivProizvoda;
    }

    public void setNazivProizvoda(String nazivProizvoda) {
        this.nazivProizvoda = nazivProizvoda;
    }

    public String getCenaProizvoda() {
        return cenaProizvoda;
    }

    public void setCenaProizvoda(String cenaProizvoda) {
        this.cenaProizvoda = cenaProizvoda;
    }

    public String getTrenutniDatum() {
        return trenutniDatum;
    }

    public void setTrenutniDatum(String trenutniDatum) {
        this.trenutniDatum = trenutniDatum;
    }

    public String getTrenutnoVreme() {
        return trenutnoVreme;
    }

    public void setTrenutnoVreme(String trenutnoVreme) {
        this.trenutnoVreme = trenutnoVreme;
    }

    public String getTotalnaKolicina() {
        return totalnaKolicina;
    }

    public void setTotalnaKolicina(String totalnaKolicina) {
        this.totalnaKolicina = totalnaKolicina;
    }

    public int getTotalnaCena() {
        return totalnaCena;
    }

    public void setTotalnaCena(int totalnaCena) {
        this.totalnaCena = totalnaCena;
    }
}
