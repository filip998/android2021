package model;

public class Administrator extends Korisnik {

    public Administrator(String id, String ime, String prezime, String username, String password, String image, boolean blokiran, String uloga) {
        super(id, ime, prezime, username, password, image, blokiran, uloga);
    }

}
