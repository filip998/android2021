package model;

import java.util.ArrayList;
import java.util.Date;

public class Prodavac extends Korisnik {

    private Date poslujeOd;
    private String email;
    private String adresa;
    private String naziv;
    private ArrayList<Artikal>listaArtikala;
    private ArrayList<Akcija>listaAkcija;


    public Prodavac(String id, String ime, String prezime, String username, String password, String profileImage, boolean blokiran, String uloga, Date poslujeOd, String email, String adresa, String naziv, ArrayList<Artikal> listaArtikala, ArrayList<Akcija> listaAkcija) {
        super(id, ime, prezime, username, password, profileImage, blokiran, uloga);
        this.poslujeOd = poslujeOd;
        this.email = email;
        this.adresa = adresa;
        this.naziv = naziv;
        this.listaArtikala = listaArtikala;
        this.listaAkcija = listaAkcija;
    }

    public Date getPoslujeOd() {
        return poslujeOd;
    }

    public void setPoslujeOd(Date poslujeOd) {
        this.poslujeOd = poslujeOd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Artikal> getListaArtikala() {
        return listaArtikala;
    }

    public void setListaArtikala(ArrayList<Artikal> listaArtikala) {
        this.listaArtikala = listaArtikala;
    }

    public ArrayList<Akcija> getListaAkcija() {
        return listaAkcija;
    }

    public void setListaAkcija(ArrayList<Akcija> listaAkcija) {
        this.listaAkcija = listaAkcija;
    }
}
